Name:      ipmiproxy
Version:   20.7.3
Release:   2%{?dist}
Summary:   Python wrapper around ipmitool
Vendor:    CERN
License:   GPL
Group:     System Environment/Base
URL:       https://gitlab.cern.ch/hw/ipmiproxy
Source:    %{name}-%{version}.tar.gz

BuildArch:      noarch
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root

AutoReq: no
BuildRequires:  python3-devel
BuildRequires:  python3-setuptools
Requires:       python3 >= 3.6
Requires:       ipmitool

%description
Python wrapper around ipmitool

%prep
%setup -q

%build
CFLAGS="%{optflags}" %{__python3} setup.py build

%install
%{__rm} -rf %{buildroot}
%{__python3} setup.py install --skip-build --root %{buildroot}

# Install executable
mkdir -p %{buildroot}%{_bindir}
install -m 755 bin/ipmiproxy  %{buildroot}%{_bindir}

%clean
%{__rm} -rf %{buildroot}

%files
%defattr(-,root,root)
%{_bindir}/ipmiproxy
%{python3_sitelib}/*
%doc AUTHORS CHANGELOG COPYING

%changelog
* Thu Jul 16 2020 - Herve Rousseau <hroussea@cern.ch> - 20.7.3-1
- Adapt for CentOS8 (python3)

* Wed Oct 30 2019 - Luca Gardi <luca.gardi@cern.ch> - 19.10.4-1
- Initial release
